-- 1. Виведіть список усіх співробітниць, які приєдналися 01.01.1990 або після 01.01.2000
SELECT emp_no, first_name, last_name, gender, hire_date FROM employees
WHERE (hire_date='1990-01-01' OR hire_date>='2000-12-31') AND gender='F';

-- 2. Покажіть імена всіх співробітників, які мають однакові ім’я та прізвище
SELECT first_name, last_name FROM employees
WHERE first_name = last_name;

-- 3. Покажіть номери співробітників 10001, 10002, 10003 і 10004. 
-- Виберіть стовпці: first_name, last_name, gender, hire_date.
SELECT first_name, last_name, gender, hire_date FROM employees
WHERE emp_no BETWEEN 10001 and 10004;

-- 4. Виберіть назви всіх департаментів, назви яких мають букву «а» на будь-якій позиції 
-- або «е» на другому місці.
SELECT * FROM departments
WHERE dept_name LIKE ('%a%') OR dept_name LIKE('_e%');

-- 5. Покажіть співробітників, які відповідають наступному опису: Йому було 45 років, 
-- коли його прийняли на роботу, він народився в жовтні і був прийнятий на роботу в неділю
SELECT first_name, last_name, gender, birth_date, hire_date FROM employees
WHERE TIMESTAMPDIFF(YEAR, birth_date, hire_date)=45 AND gender='M' AND MONTH(birth_date)= 10
AND DAYOFWEEK(hire_date)=1;

-- 6. Покажіть максимальну річну зарплату в компанії після 01.06.1995.
SELECT MAX(salary) AS max_salary FROM salaries
WHERE from_date>= '1995-06-01';

-- 7. У таблиці dept_emp покажіть кількість співробітників за департаментами (dept_no). 
-- To_date має бути більшим за current_date. Покажіть департаменти з понад 13 000 співробітників. 
-- Відсотртуйте за кількістю працівників.
SELECT dept_no, COUNT(DISTINCT emp_no) AS count_emp FROM dept_emp
WHERE to_date>CURDATE()
GROUP BY dept_no
HAVING count_emp>= '13000'
ORDER BY count_emp DESC;

-- 8. Покажіть мінімальну та максимальну зарплати по працівникам.
SELECT DISTINCT emp_no, MIN(salary) AS min_salary, MAX(salary) AS max_salary FROM salaries
GROUP BY emp_no
ORDER BY emp_no ASC;
